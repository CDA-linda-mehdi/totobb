package calc;

public class CalculatriceTableau {
	public static int sommeElements(int tab[]) {

		int resultat = 0;

		for (int i = 0; i < tab.length; i++) {
			resultat = resultat + tab[i];
		}
		return resultat;

	}
}